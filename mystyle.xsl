<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:import href="/usr/share/xml/docbook/xsl-stylesheets-1.78.1/fo/docbook.xsl"/>

  <xsl:template match="title" mode="chapter.titlepage.recto.auto.mode">  
    <fo:block xmlns:fo="http://www.w3.org/1999/XSL/Format" 
	      xsl:use-attribute-sets="chapter.titlepage.recto.style" 
	      margin-left="{$title.margin.left}"
	      color="#ff0000"
	      font-size="21pt"
	      font-weight="bold"
	      font-family="{$title.font.family}">
      <xsl:call-template name="component.title">
	<xsl:with-param name="node" select="ancestor-or-self::chapter[1]"/>
      </xsl:call-template>
    </fo:block>
  </xsl:template>

<!-- bridgehead -->

<xsl:attribute-set name="section.title.properties">
  <!--xsl:attribute name="font-family">
    <xsl:value-of select="$title.font.family"/>
  </xsl:attribute-->
  <xsl:attribute name="font-weight">bold</xsl:attribute>
    <xsl:attribute name="color">#ff0000</xsl:attribute>
  <!-- font size is calculated dynamically by section.heading template -->
  <xsl:attribute name="keep-with-next.within-column">always</xsl:attribute>
  <xsl:attribute name="text-align">left</xsl:attribute>
  <xsl:attribute name="margin-top">3em</xsl:attribute>
  <xsl:attribute name="space-before.minimum">0.8em</xsl:attribute>
  <xsl:attribute name="space-before.optimum">1.0em</xsl:attribute>
  <xsl:attribute name="space-before.maximum">1.2em</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="section.title.level4.properties">
  <xsl:attribute name="font-weight">normal</xsl:attribute>
  <xsl:attribute name="font-style">normal</xsl:attribute>
  <xsl:attribute name="color">#ff000</xsl:attribute>
  <xsl:attribute name="font-family">Argor Got Scaqh</xsl:attribute>
</xsl:attribute-set>

<xsl:template match="emphasis[@role='bold']">
  <fo:block font-weight="bold">
    <xsl:apply-templates/>
  </fo:block>
</xsl:template>

<xsl:attribute-set name="admonition.properties">
  <!--xsl:attribute name="border">0.5pt solid gray</xsl:attribute-->
  <xsl:attribute name="background-color">#d9d9d9</xsl:attribute>
  <xsl:attribute name="margin-bottom">3em</xsl:attribute>
  <xsl:attribute name="padding">0.1in</xsl:attribute>
</xsl:attribute-set>

<xsl:template match="processing-instruction('hard-pagebreak')">
  <fo:block break-after='page'/>  
</xsl:template>

</xsl:stylesheet>

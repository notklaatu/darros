build:
	mkdir $@

help:
	@echo "make concat   DO THIS FIRST. Concatenates files for processing."
	@echo "make release  Increment git tag."
	@echo "make html     Generate book as HTML."
	@echo "make txt      Generate book as plain text."
	@echo "make epub     Generate book as an epub."
	@echo "make pdf      Generate book as an PDF."
	@echo "make clean    Remove tmp files."

release:
	CURVER=$(shell git describe --tags --exact-match --abbrev=0)	
	@sed 's/'$(CURVER)'/'$(version)'/' VERSION || exit "version string replacement failure"
	@git tag $(version)

echo:
	@echo "Version updated to $(version)"

coverp:	img/cover_front.png
	@convert img/cover_front.png build/front.pdf

concat: header.xml footer.xml build
	@test -d build || mkdir build
	cat header.xml \
	src/chapter1.xml \
	src/chapter2.xml \
	src/chapter3.xml \
	src/chapter4.xml \
	src/chapter5.xml \
	src/chapter6.xml \
	src/chapter7.xml \
	src/chapter8.xml \
	src/chapter9.xml \
	src/chapter10.xml \
	src/chapter11.xml \
	src/chapter12.xml \
	src/appendix.xml \
	src/encalc.xml \
	src/bibliography.xml \
	src/colophon.xml \
	src/OGLv1.0a.xml \
	footer.xml > tmp.xml

html:	build concat
	@mkdir dist  || true
	xmlto --skip-validation -o build html-nochunks tmp.xml
	@mv build/tmp.html dist/tomeOfDarros.html

txt:	build concat
	@mkdir dist  || true
	xmlto --skip-validation -o build txt tmp.xml
	@mv build/tmp.txt dist/tomeOfDarros.txt

epub:	build concat
	@mkdir build || true
	@mkdir dist  || true
	xmlto --skip-validation -o build epub tmp.xml
	@mv build/tmp.epub dist/tomeOfDarros.epub

pdf:	build concat coverp
	@mkdir build || true
	@mkdir dist  || true
	xsltproc --output build/tmp.fo \
	 --stringparam page.width 8in \
	 --stringparam page.height 10in \
	 --stringparam body.font.family "Liberation Sans" \
	 --stringparam title.font.family "Junction" \
	 --stringparam bridgehead.font.family "Junction" \
	 --stringparam body.font.master 10 \
	 --stringparam body.font.size 10 \
	 --stringparam page.margin.inner .5in \
	 --stringparam page.margin.outer .5in \
	 --stringparam page.margin.top .45in \
	 --stringparam page.margin.bottom .45in \
	 --stringparam title.margin.left 0 \
	 --stringparam title.start.indent 0 \
	 --stringparam body.start.indent 0 \
	 --stringparam chapter.autolabel 0 \
	mystyle.xsl tmp.xml
	fop -c rego.xml build/tmp.fo build/tmp.pdf
	pdftk build/front.pdf build/tmp.pdf cat output dist/tomeOfDarros.pdf || mv build/tmp.pdf dist/tomeOfDarros.pdf

clean:	build
	@rm -rf build
	@rm -rf tmp*xml

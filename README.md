# Tome of Darros

*A 10 to 15 hour adventure for Level 1 and 2 characters.*

This is a short adventure for the [Pathfinder](http://paizo.com/pathfinderRPG/), a tabletop role-playing game (RPG). Since Pathfinder is a fork of **Dungeons and Dragons**, this adventure is easy to adapt for the latest edition of [D&D](http://dnd.wizards.com/), as well.

## Elevator pitch

An ancient book of spells, far beyond the comprehension of even the wisest wizard, has been stolen from the Athenaeum. A brave group of new adventurers venture out to solve the mystery of who has stolen it, why, and finally to recover it.


## How do I build this?

What you are looking at right now is the source code for the adventure. If you want to download and play this adventure, it's much easier to just download a copy from http://www.drivethrurpg.com/product/230035.

### No really, I want to build this.

If, for whatever reason, you want to build this thing from source, you can do that. This was written on Slackware Linux and I've lazily structured it around Slackware's default Docbook install. If you are familiar with XML and Docbook, you should have no problem.

The build requirs:

* [Docbook](http://docbook.org)
* [xsltproc](http://xmlsoft.org/XSLT/index.html)
* [FOP 2](https://xmlgraphics.apache.org/fop/)

These are the steps I would take to do the build, were I you:

1. Clone this repo
2. Verify the Docbook paths in the ``header.xml`` file are appropriate for your system.
3. Verify that you have the two fonts required: [DejaVu Sans](https://dejavu-fonts.github.io/) and [Junction](https://www.theleagueofmoveabletype.com/). These are the fonts specified by the [Dungeon Masters Guild](https://www.dmsguild.com/) templates from **Wizards of the Coast**, so they aren't literally required (edit ``GNUmakefile`` to change them), but they make it look nice.
4. ``make clean concat pdf``
5. Find the PDF in the ``dist`` directory.

## License

It's all open source and free culture. See LICENSE file for details.